import { Component, Input, OnInit } from '@angular/core';
import { CadastrarService } from './cadastrar.service';

@Component({
  selector: 'app-cadastrar',
  templateUrl: './cadastrar.component.html',
  styleUrls: ['./cadastrar.component.scss']
})
export class CadastrarComponent implements OnInit {

  constructor(private service: CadastrarService) { }

  nome : string = "";
  valor : string = "";
  descricao : string = "";

  atualizaNome(event : Event) { this.nome = (event.target as HTMLInputElement).value }
  atualizaValor(event : Event) { this.valor = (event.target as HTMLInputElement).value }
  atualizaDesc(event : Event) { this.descricao = (event.target as HTMLInputElement).value }

  enviarDados() {
    const product = {
      title : this.nome,
      price : this.valor,
      description : this.descricao 
    }
    this.service.cadastrar(product).subscribe();
    return alert("Cadastrado com sucesso!");
  }

  ngOnInit(): void {
  }

}
