import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CadastrarService {

  constructor(private http: HttpClient) { }

  private readonly API = "https://banco-dados-teste.glitch.me/api/produtos";

  cadastrar(product : any) {
    return this.http.post(this.API, product);
  }

}
