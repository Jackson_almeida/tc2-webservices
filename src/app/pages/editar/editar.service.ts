import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class EditarService {

  constructor(private http: HttpClient) { }

  private readonly API = "https://banco-dados-teste.glitch.me/api/produtos";

  editar(product : any) {
    return this.http.put(`${this.API}/${''}`, product)
  };

}
