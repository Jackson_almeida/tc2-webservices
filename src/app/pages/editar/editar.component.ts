import { Component, OnInit } from '@angular/core';
import { EditarService } from './editar.service';

@Component({
  selector: 'app-editar',
  templateUrl: './editar.component.html',
  styleUrls: ['./editar.component.scss']
})
export class EditarComponent implements OnInit {

  constructor(private service: EditarService) { }

  nome : string = "";
  valor : string = "";
  descricao : string = "";

  atualizaNome(event : Event) { this.nome = (event.target as HTMLInputElement).value }
  atualizaValor(event : Event) { this.valor = (event.target as HTMLInputElement).value }
  atualizaDesc(event : Event) { this.descricao = (event.target as HTMLInputElement).value }

  editarDados() {
    const product = {
      title : this.nome,
      price : this.valor,
      description : this.descricao 
    }
    this.service.editar(product).subscribe();
  }

  ngOnInit(): void {
  }

}
