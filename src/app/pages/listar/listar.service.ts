import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ListarService {

  constructor(private http: HttpClient) { }

  private readonly API = "https://banco-dados-teste.glitch.me/api/produtos";

  list() {
    return this.http.get(this.API);
  }

  del(id : any) {
    return this.http.delete(`${this.API}/${id}`);
  }

}
