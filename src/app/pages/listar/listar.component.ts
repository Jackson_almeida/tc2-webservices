import { R3TargetBinder } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { ListarService } from './listar.service';

@Component({
  selector: 'app-listar',
  templateUrl: './listar.component.html',
  styleUrls: ['./listar.component.scss']
})
export class ListarComponent implements OnInit {

  constructor(private service: ListarService) { }

  data : any;

  remover(id : any) : any {    
    this.service.del(id).subscribe();
    alert('Removido com sucesso!');
    return this.service.list().subscribe(data => {this.data = data});
  }

  ngOnInit(): void {
    this.service.list().subscribe(data => {
      this.data = data;
    });   
  }

}
