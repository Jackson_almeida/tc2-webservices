import { ModuleWithProviders } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ListarComponent } from './pages/listar/listar.component';
import { CadastrarComponent } from './pages/cadastrar/cadastrar.component';
import { EditarComponent } from './pages/editar/editar.component';

const APP_ROUTES: Routes = [
    { path: 'cadastrar', component: CadastrarComponent },
    { path: 'editar', component: EditarComponent },
    { path: '', component: ListarComponent }
];

// export const routing: ModuleWithProviders = RouterModule.forRoot(APP_ROUTES);